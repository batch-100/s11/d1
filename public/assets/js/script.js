//comments

//comments are sections in the code that is not read by the language
// it is for the user's to know what the code does
// in javascript, there are two types of comments
/*
	1. the single line comment, denoted by //
	2. the multi line comment, denonet by slash an asterisk
 */
// yung alert pwede gawing pang check if connected na siya sa webpage
// alert ("hellow world")

/*Syntax and Statements

Syntax contains the rules needed to create a statement in a programming language
Statements are instructions we tell the application to perform.
statements end with a semicolon (;) as best practice


Variables and contants
	-are containers for data that can be used to store information and retrieve or manipulate in the future

	Variables can container values that can change as the program runs
	Constant contain values that cannot change as the program runs


	To create a variable, we use the "let" keyword (keywords are special words in a programming language)


	To create a constant, we use the const keyword
	*/

let productName = "Desktop Computer"; //"desktop computer" is a string. String usually have double  quotes
let productPrice = 18999; //number do not need double quoutes
const PI = 3.1416;


/*Rules in naming variables and constants:
	1. Variable names start with lowercase letters, for multiple words, we use camelCase
	2. Variable names should be descruptive of what values it can contain
	

Console - is a part of a browser wherein outputs can be displayed. it can be accessded via the console tab in any browser



*/
console.log(productName); //console is an obkect in JS and .log is a function  that allows the writing of the output
console.log(productPrice);
console.log(PI);
console.log("hellow world");
console.log("I am selling a "+ productName); 


/*
Data Types - are the type of a data a variable can handle
	1. string combination of alphanumeric characters
	2. number - numbers (+,- decimal ,ex: 100, -9, 3.1416)
	3. boolean - true or false
	4. Object - combination of data
	5. BigInt *not used often
	6. Symbol - *not used often
	7. Undefined - only appears if a variable was not assigned to any value
	8. Null - intentional walang kang inassign na value


 */
/*String*/
let fullName = "Brand b Brandon";
let schoolName = "Zuitt Coding Bootcamp";
let userName = "brandon00";

/*number*/
let age = 28;
let numberofPets = 5;
let desiredGrade = 98.5;

/*boolean*/
let isSingle = true; //no need for quotation marks
let hasEaten = false;

/*Undefined*/
let petName; //undefined siya kasi walang inassign na value

//Null

let grandChildName = null; //you dont have any grandchildren yet;

//object
let person = {
	firstName : "Jobert", //the indicates that there are still more properties after this line
	lastName : "Boyd",
	age : 15,
	petName : "Whitey" //you dont need a comma for the last property
}; //semicolon is after the closing curly brace


/*
Operators - allow us to perform operations or evaluate results
There are 5 main types of operators:
	1. Assignment
	2. Arithmetic 
	3. Comparison
	4. Relational
	5. Logical

 */

/*Assignment operators uses the  = symbol -> it assigns a value to a variable
*/

let num1 = 28;
let num2 = 75;

//Arithmetic operator has 5 operations
let sum = num1 + num2;
console.log(sum);
let difference = num1 - num2;
console.log(difference);
let product = num1 * num2;
console.log(product);
let quotient = num2 / num1;
console.log(quotient);
let remainder = num2 % num1; //% is called the modulo operator
console.log(remainder);


//you can actually combine the assignment and arithmetic operators
let num3 = 17;

//num3 = num3 -4;
num3 -=4; // this is the same as num3 = num3 - 4; this is a shorthand  version

//Special arithmetic operators ++ and -- (increment and decrement +1 and -1)

//Comparison and Relational

//comparison compares two values if they are equal or not
//==, ===, !=, !==
let numA = 65;
let numB = 65;

console.log(numA==numB);

let statement1 = "true";
let statement2 = true;

console.log(statement1 == statement2);
console.log(statement1 === statement2);

//Relational comapre two numbers if they are equal or not
let numC = 25;
let numD = 45;

console.log (numC > numD);
console.log (numC < numD);
console.log (numC >= numD);

//logical - compares two boolean values
////AND (&&) or (||), NOT (!)

let isTall = false;
let isDark = true;
let isHandsome = true;
let didPassStandard = isTall && isDark;
console.log(didPassStandard);


let didPassLowerStandard = isTall || isDark;
console.log(didPassLowerStandard);


console.log(!isTall);

let result = isTall || (isDark && isHandsome);
console.log(result);

/*
Functions - are groups of statements that perform a single action to prevent  code  duplication 
a function has two main concepts
	1. function declaration (definition)
	2. function invocation (calling)
 */
//Function Declaration

function createFullName (fname, mName,lName){
	return fname + mName + lName;
 }

/*
function is a keyword in JS that says that it is a function createFullName is the name of the function, it should descriptive of  what it does fName
mName, lName are called parameters, these are the inputs that the function needs in order to work. Note parameters are optional, meaning you can create a function withou any parameters
return is a keyword in JS that returns the resulting value after the function is complete run.
Note: this is also optional is you don't want any returning values.
 
fName, mName lName are placeholders for values and is only used inside the function

 */

//function invocation
let fullName1 = createFullName("Brandon","Ray","Smith"); //BrandonRaySmith
console.log(fullName1);
let fullName2 = createFullName ("John", "robert", "smith"); //JohnRobertSmit
console.log(fullName2);


let fN = "Jobert";
let mN = "Bob";
let lN = "Garcia";
let fullName3 = createFullName(fN, mN, lN);
console.log("My Name is" + fullName3); //JobertBobGarcia

let fullName4 = createFullName(mN, fN, lN);
console.log(fullName4);

//Mini exercise
//Create a function that gets two numbers and returns twice the sum of the two numbers
//the function name is addTwo.
//



function addTwo(firstNum, secondNum){
	return 2*(firstNum + secondNum);
}

//parang need gayahin ung format ng function pag tatwagin sa console
console.log(addTwo(54,106));



function createFullName (fname, mName,lName){
	return fname + mName + lName;
}

let name = "Areej";
let email = "agrrollon@gmail.com";
let job = "student";
let hobby = "dancing";



let fullName3 = createFullName(fN, mN, lN);
console.log("My Name is" + fullName3);